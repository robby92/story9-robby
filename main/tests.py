from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from .views import *
from django.apps import apps
from .apps import MainConfig
from django.http import HttpRequest



# Create your tests here.
class Story9Test(TestCase):

    def test_app_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, Landing)

    def test_function_logout(self):
        found = resolve('/logout')
        self.assertEqual(found.func, logout)
        
    def test_function_signup(self):
        found = resolve('/signup')
        self.assertEqual(found.func, signup)

    def test_function_main(self):
        found = resolve('/main')
        self.assertEqual(found.func, main)

    def test_landing_page_is_using_correct_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'main/landing.html')

    def test_login_page_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    
    def test_page_template_logout(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code,302)
    
    def test_story9_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'main/landing.html')


    def test_signup_page_url_is_exist(self):
        response = Client().get('/signup')
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(MainConfig.name, 'main')
        self.assertEqual(apps.get_app_config('main').name, 'main')

 